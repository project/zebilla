== Zebilla theme ==

Project URL: http://drupal.org/project/zebilla
Author: Samuel Ayela
Author URL: http://www.samuelayela.com/

Project details:

**Zebilla is a clean, tableless, multi-column Drupal theme.

**If you find a bug please report it to http://drupal.org/project/issues/zebilla

**For documentation on the Zebilla theme, visit http://samuelayela.com/node/4
